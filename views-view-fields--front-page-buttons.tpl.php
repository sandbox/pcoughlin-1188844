<?php

/**

 * @file views-view-fields.tpl.php

 * Default simple view template to all the fields as a row.

 *

 * - $view: The view in use.

 * - $fields: an array of $field objects. Each one contains:

 *   - $field->content: The output of the field.

 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.

 *   - $field->class: The safe class id to use.

 *   - $field->handler: The Views field handler object controlling this field. Do not use

 *     var_export to dump this object, as it can't handle the recursion.

 *   - $field->inline: Whether or not the field should be inline.

 *   - $field->inline_html: either div or span based on the above flag.

 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.

 *   - $field->wrapper_suffix: The closing tag for the wrapper.

 *   - $field->separator: an optional separator that may appear before a field.

 *   - $field->label: The wrap label text to use.

 *   - $field->label_html: The full HTML of the label to use including

 *     configured element type.

 * - $row: The raw result object from the query, with all data it fetched.

 *

 * @ingroup views_templates

 */

?>

        <?php if($fields['field_button']->content){?>
	<?php $color = "white";?>
	<?php }else{?>
         <?php $color = "black";?>
	<?php }?>

<div style="position:absolute;margin:0;padding:0;">
<?php echo $fields['field_button']->content;?>
</div>
<div style="position:absolute;width:100%;margin:0;padding:0;">
<a href="<?php print $fields['field_button_url']->content;?>">
<div style="height:87px;" class="<?php echo $color;?>">	

<?php 
if(isset($fields['field_button_line1'])){ print $fields['field_button_line1']->content;} 
?>
<?php 
if(isset($fields['field_button_line2'])){ print $fields['field_button_line2']->content;} 
?>

<?php 
if(isset($fields['field_button_line3'])){ print $fields['field_button_line3']->content;} 
?>

<?php 
if(isset($fields['field_button_line4'])){ print $fields['field_button_line4']->content;} 
?>

</div>
</a>
</div>



