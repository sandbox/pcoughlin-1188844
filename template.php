<?php


function colorado_preprocess_node(&$vars) {
  //default template suggestions for all nodes
  $vars['template_files'] = array();
  $vars['template_files'][] = 'node';

  if (drupal_is_front_page()) {
    $vars['template_files'][] = 'node-front';
  }

  //individual node being displayed
  if(!empty($vars['page'])) {
    $vars['template_files'][] = 'node-page';
    $vars['template_files'][] = 'node-'.$vars['node']->type.'-page';
    $vars['template_files'][] = 'node-'.$vars['node']->nid.'-page';
  }
  //multiple nodes being displayed on one page in either teaser
  //or full view
  else {
    //template suggestions for nodes in general
    $vars['template_files'][] = 'node-'.$vars['node']->type;
    $vars['template_files'][] = 'node-'.$vars['node']->nid;

    //template suggestions for nodes in teaser view
    //more granular control
    if(!empty($vars['teaser'])) {
      $vars['template_files'][] = 'node-'.$vars['node']->type.'-teaser';
      $vars['template_files'][] = 'node-'.$vars['node']->nid.'-teaser';
    }
    if(!empty($vars['event'])) {
      $vars['template_files'][] = 'node-'.$vars['node']->type.'-event';
      $vars['template_files'][] = 'node-'.$vars['node']->nid.'-event';
    }
    if(!empty($vars['staff-bio'])) {
      $vars['template_files'][] = 'node-'.$vars['node']->type.'-staff-bio';
      $vars['template_files'][] = 'node-'.$vars['node']->nid.'-staff-bio';
    }
 if(!empty($vars['staff_bio'])) {
     $vars['template_files'][] = 'node-'.$vars['node']->type.'-staff_bio';
     $vars['template_files'][] = 'node-'.$vars['node']->nid.'-staff_bio';
   }
  }
//print '<pre>All vars='.print_r($vars,true).'</pre><br>';
  //echo "<pre>" . print_r($vars['template_files'], TRUE) . "</pre>"; //TESTING
}
